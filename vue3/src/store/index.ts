import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userId: '',
    username: '',
    role: '',
    deposit: null
  },
  mutations: {
    setUserId (state: any, id: string) {
      state.userId = id
    },
    setUsername (state: any, name: string) {
      state.username = name
    },
    setRole (state: any, role: string) {
      state.role = role
    },
    setDeposit (state: any, deposit: number) {
      state.deposit = deposit
    }
  },
  getters: {
    userId: state => {
      return state.userId
    },
    username: state => {
      return state.username
    },
    role: state => {
      return state.role
    },
    deposit: state => {
      return state.deposit
    }
  },
  actions: {
  },
  modules: {
  }
})
