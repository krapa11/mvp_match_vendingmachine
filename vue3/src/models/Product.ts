export interface IProduct {
  _id: string;
  userId: string;
  name: string;
  amountAvailable: number;
  cost: number;
}
