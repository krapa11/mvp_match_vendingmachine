import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Login from '../views/Login.vue'
import DashboardWrapper from '../views/DashboardWrapper.vue'
import Dashboard from '../views/Dashboard.vue'
import EditProduct from '../views/EditProduct.vue'
import NotFoundPage from '../views/NotFoundPage.vue'
import SessionService from '../services/SessionService'

Vue.use(VueRouter)

function allowedToShowLogin (): boolean {
  try {
    const token = SessionService.getVerifiedAPIToken()
    if (token != null) {
      return false
    }
  } catch (error) {
  }
  return true
}

function allowedToShowProtectedRoute (): boolean {
  try {
    const token = SessionService.getVerifiedAPIToken()
    if (token != null) {
      return true
    }
  } catch (error) {
  }
  return false
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    beforeEnter: (to, from, next) => {
      if (allowedToShowLogin()) {
        next()
      } else {
        next('/dashboard')
      }
    }
  },
  {
    path: '/dashboard',
    name: 'DashboardWrapper',
    component: DashboardWrapper,
    beforeEnter: (to, from, next) => {
      if (allowedToShowProtectedRoute()) {
        next()
      } else {
        next('/')
      }
    },
    children: [
      {
        path: '',
        name: 'Dashboard',
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "dashboard" */ '../views/Dashboard.vue')
      },
      {
        path: '/edit-product/:id',
        name: 'EditProduct',
        component: () => import(/* webpackChunkName: "editProduct" */ '../views/EditProduct.vue')
      },
      {
        path: '/settings',
        name: 'EditUser',
        component: () => import(/* webpackChunkName: "EditUser" */ '../views/EditUser.vue')
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '*',
    name: 'NotFoundPage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: NotFoundPage
  }
]

const router = new VueRouter({
  routes
})

export default router
