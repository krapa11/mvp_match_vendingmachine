import axios, { AxiosResponse, AxiosRequestConfig, AxiosError } from 'axios'
import SessionService from '../services/SessionService'
import jwtDecode, { JwtPayload } from 'jwt-decode'

// before every request, check if sent access token is valid
// if it is expired, get new token by refresh token and use the new auth-token for current request
axios.interceptors.request.use((req: AxiosRequestConfig) => {
  return new Promise((resolve, reject) => {
    if (req.data?.refreshToken != null) {
      // is refresh token request
      resolve(req)
      return
    }

    if (req.headers?.Authorization == null) {
      // is public resource
      resolve(req)
      return
    }

    let token = req.headers?.Authorization.toString()
    if (token == null) {
      // no token set
      resolve(req)
      return
    }

    token = token!.replace('Bearer ', '')
    const decoded = jwtDecode<JwtPayload>(token)
    const tokenExpiresAt = decoded.exp
    if (tokenExpiresAt == null) {
      // token is not expired
      console.log('token is not expired')
      resolve(req)
      return
    }
    const now = new Date().getTime() / 1000
    const offset = 0
    if (tokenExpiresAt! < (now + offset)) {
      // token is or will soon be expired
      // renew it with refresh token
      console.log('token is or will soon be expired')
      console.log('get new token with refresh-token')
      const config = {
        headers: {
          Authorization: 'Bearer ' + token
        }
      }
      const data = {
        refreshToken: SessionService.getCookie('refresh-token')
      }
      axios.post('/api/refresh-token', data, config)
        .then(refreshTokenResp => {
          if (refreshTokenResp.data.token != null) {
            // set new token for current request
            req.headers!.Authorization = 'Bearer ' + refreshTokenResp.data.token
            SessionService.setAPIToken(refreshTokenResp.data.token)
            resolve(req)
          } else {
            resolve(req)
          }
        })
        .catch(refreshTokenRespError => {
          resolve(req)
        })
    } else {
      resolve(req)
    }
  })
})

export default axios
