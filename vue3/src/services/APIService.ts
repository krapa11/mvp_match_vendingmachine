import axios from './axios'
import { IProduct } from '../models/Product'
import router from '../router'
import { AxiosError } from 'axios'
import SessionService from '../services/SessionService'

interface IResponse {
  success: boolean;
  error?: string;
}

interface ILoginResponse extends IResponse {
  alreadyLoggedIn: boolean;
  token: string;
  refreshToken: string;
}

type IRegisterResponse = IResponse

interface IDashboardResponse extends IResponse {
  user: {
    _id: string;
    name: string;
    deposit: number;
    role: string;
  }
}
interface IDepositResponse extends IResponse {
  deposit: number;
  add: number;
}
interface IProductsResponse extends IResponse {
  products: IProduct[]
}
interface IProductResponse extends IResponse {
  product: IProduct
}

export interface IBuyProductsParam {
  products: [
    {
      id: string;
      quantity: number;
    }
  ]
}
interface IBuyProductsResponse extends IResponse {
  spent: number;
  change: {
    five: number;
    ten: number;
    twenty: number;
    fifty: number;
    hundert: number;
  };
  products: IProduct[];
  deposit: {
    now: number;
    before: number;
  }
}

export interface IAddProductsParam {
  amountAvailable: number;
  cost: number;
  name: string;
}
type IAddProductsResponse = IResponse

export interface IPutProductsParam {
  amountAvailable?: number;
  cost?: number;
  name?: string;
}
type IPutProductsResponse = IResponse

type IDeleteProductsResponse = IResponse

interface IGetUserResponse extends IResponse {
  user: {
    name: string;
    role: string;
  }
}
export interface IEditUserParams {
  name?: string;
  password?: string;
}
interface IEditUserResponse extends IResponse {
  user: {
    name: string;
  }
}

class APIService {
  private url = '/api/'

  private getAuthenticationHeader (token: string) {
    const config = {
      headers: {
        Authorization: 'Bearer ' + token
      }
    }

    return config
  }

  private handleErrorOnProtectedRoute (error: AxiosError) {
    if (error.response?.status === 401) {
      SessionService.removeAPIToken()
      router.push('/')
    }
  }

  loginUser (name: string, password: string): Promise<ILoginResponse> {
    return new Promise((resolve, reject) => {
      const data = {
        name: name,
        password: password
      }
      axios.post(this.url + 'login', data)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true,
              alreadyLoggedIn: resp.data.alreadyLoggedIn ?? false,
              token: resp.data.token,
              refreshToken: resp.data.refreshToken
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch(error => {
          reject(Error(error))
        })
    })
  }

  logoutUser (token: string): Promise<IResponse> {
    const config = this.getAuthenticationHeader(token)
    return new Promise((resolve, reject) => {
      axios.post(this.url + 'logout', {}, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch(error => {
          reject(Error(error))
        })
    })
  }

  logoutAllUsers (name: string, password: string): Promise<IResponse> {
    const data = {
      name: name,
      password: password
    }
    return new Promise((resolve, reject) => {
      axios.post(this.url + 'logout/all', data)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch(error => {
          reject(Error(error))
        })
    })
  }

  registerUser (name: string, password: string, isSeller: boolean): Promise<IRegisterResponse> {
    return new Promise((resolve, reject) => {
      const data = {
        name: name,
        password: password,
        isSeller: isSeller
      }
      axios.post(this.url + 'register', data)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true
            })
          } else {
            console.log(resp)

            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch(error => {
          reject(Error(error))
        })
    })
  }

  dashboardOfUser (token: string): Promise<IDashboardResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.get(this.url + 'dashboard', config)
        .then(resp => {
          if (resp.data.success === true) {
            const user = resp.data.user
            resolve({
              success: true,
              user: {
                _id: user._id,
                name: user.name,
                deposit: user.deposit,
                role: user.role
              }
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  depositCoins (token: string, coins: any): Promise<IDepositResponse> {
    const config = this.getAuthenticationHeader(token)

    const data = {
      five: coins.fiveCentCoins,
      ten: coins.tenCentCoins,
      twenty: coins.twentyCentCoins,
      fifty: coins.fiftyCentCoins,
      hundret: coins.hundertCentCoins
    }

    return new Promise((resolve, reject) => {
      axios.post(this.url + 'deposit', data, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true,
              deposit: resp.data.deposit,
              add: resp.data.add
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  resetDeposit (token: string): Promise<IResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.post(this.url + 'reset', {}, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  loadProducts (): Promise<IProductsResponse> {
    return new Promise((resolve, reject) => {
      axios.get(this.url + 'product')
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true,
              products: resp.data.products
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch(error => {
          reject(Error(error))
        })
    })
  }

  loadProduct (id: string): Promise<IProductResponse> {
    return new Promise((resolve, reject) => {
      axios.get(this.url + 'product/' + id)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true,
              product: resp.data.product
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch(error => {
          reject(Error(error))
        })
    })
  }

  buyProducts (token: string, products: IBuyProductsParam): Promise<IBuyProductsResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.post(this.url + 'buy', products, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true,
              spent: resp.data.spent,
              change: resp.data.change,
              products: resp.data.products,
              deposit: resp.data.deposit
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  addProduct (token: string, productData: IAddProductsParam): Promise<IAddProductsResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.post(this.url + 'product', productData, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  putProduct (token: string, id: string, productData: IPutProductsParam): Promise<IPutProductsResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.put(this.url + 'product/' + id, productData, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  deleteProduct (token: string, id: string): Promise<IDeleteProductsResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.delete(this.url + 'product/' + id, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  getUser (token: string): Promise<IGetUserResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.get(this.url + 'user', config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true,
              user: resp.data.user ?? {}
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }

  editUser (token: string, data: IEditUserParams): Promise<IEditUserResponse> {
    const config = this.getAuthenticationHeader(token)

    return new Promise((resolve, reject) => {
      axios.put(this.url + 'user', data, config)
        .then(resp => {
          if (resp.data.success === true) {
            resolve({
              success: true,
              user: resp.data.user ?? {}
            })
          } else {
            reject(Error(resp.data.error ?? ''))
          }
        })
        .catch((error: AxiosError) => {
          this.handleErrorOnProtectedRoute(error)
          reject(Error(error.message))
        })
    })
  }
}

const apiService = new APIService()
export default apiService
