import jwtDecode, { JwtPayload } from 'jwt-decode'

class SessionService {
  // session storage
  private saveSessionVariable (key: string, value: string): void {
    window.sessionStorage.setItem(key, value)
  }

  private getSessionVariable (key: string): string {
    const value = window.sessionStorage.getItem(key)
    if (value == null) {
      throw Error('not set')
    }
    return value
  }

  private removeSessionVariable (key: string) {
    window.sessionStorage.removeItem(key)
  }

  // cookies
  private saveCookie (key: string, value: string): void {
    let expires = ''
    const days = 1
    const date = new Date()
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
    expires = '; expires=' + date.toUTCString()
    document.cookie = key + '=' + (value || '') + expires + '; path=/'
  }

  private deleteCookie (key: string): void {
    document.cookie = key + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;'
  }

  setAPIToken (token: string) {
    // this.saveSessionVariable('token', token)
    this.saveCookie('token', token)
  }

  getAPIToken (): string {
    // return this.getSessionVariable('token')
    return this.getCookie('token')
  }

  getVerifiedAPIToken (): string {
    const token = this.getAPIToken()
    try {
      const decoded = jwtDecode<JwtPayload>(token)
      if (decoded == null) {
        throw Error('token cannot be decoded')
      }
    } catch (error) {
      console.log(error)
      throw Error('invalid token')
    }
    return token
  }

  getAPITokenPayload (): any {
    const token = this.getAPIToken()
    try {
      const decoded = jwtDecode<JwtPayload>(token)
      return decoded
    } catch (error) {
      console.log(error)
      throw Error('invalid token')
    }
  }

  removeAPIToken (): void {
    // this.removeSessionVariable('token')
    this.deleteCookie('token')
  }

  getCookie (cname: string): string {
    const name = cname + '='
    const decodedCookie = decodeURIComponent(document.cookie)
    const ca = decodedCookie.split(';')
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i]
      while (c.charAt(0) === ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }
}

const sessionService = new SessionService()
export default sessionService
