import axios from 'axios'
import mongoose from "mongoose";
import { dbHost } from "../src/config";
import { getApiRoute } from "./services/service"
// DB models
import { User } from '../src/models/user'


// before and after
beforeAll(() => {
  console.log("before all")

  return new Promise<void>((resolve, reject) => {
    mongoose.connect(dbHost, {
      autoIndex: true,
    }, (result) => {
      console.log("connect to mongoose", result)
      resolve()
    })
  });
});

afterAll(() => {
  console.log("after all")

  mongoose.disconnect()
});


//////////////
// tests
////

// login test
test('login test', (done: any) => {
  let data = {
    name: 'buyer',
    password: 'test'
  }
  axios.post(getApiRoute("login"), data)
    .then(resp => {
      expect(resp.data.success).toBe(true)
      expect(resp.data.token).toBeDefined()
      expect(resp.data.refreshToken).toBeDefined()
      expect(resp.data.alreadyLoggedIn).toBeDefined()
      done()
    })
    .catch(reason => {
      console.log(reason);
      done("error")
    })
});
