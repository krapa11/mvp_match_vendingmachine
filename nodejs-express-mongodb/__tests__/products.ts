import axios, { AxiosError } from 'axios'
import mongoose from "mongoose";
import { dbHost } from "../src/config";
import { url, getApiRoute, getHeader } from "./services/service"
// DB models
import { User } from '../src/models/user'
import { Product } from '../src/models/product'


let seller = {
  name: "seller",
  password: "test"
}
let sellerToken: string = null

// before and after
beforeAll(() => {
  console.log("products - before all")

  return new Promise<void>((resolve, reject) => {
    mongoose.connect(dbHost, {
      autoIndex: true,
    }, (result) => {
      console.log("connected to mongoose")

      let data = seller
      axios.post(getApiRoute("login"), data)
        .then(resp => {
          sellerToken = resp.data.token
          resolve()
        })
    })
  });
});

afterAll(() => {
  console.log("products - after all")

  mongoose.disconnect()
});


//////////////
// tests
////

test('add product - unauthorized', (done: any) => {
  let data = {
    name: 'product',
    cost: 1000,
    amountAvailable: 10
  }
  axios.post(getApiRoute("product"), data, {})
    .then(resp => {
      // expect(resp.data.product.userId).toBe(data.amountAvailable)
      done("error")
    })
    .catch((reason: AxiosError) => {
      expect(reason.response.status).toBe(401)
      done()
    })
});

test('add new product', async () => {
  let data = {
    name: 'product' + new Date().getTime(),
    cost: 1000,
    amountAvailable: 10
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(true)
  expect(resp.data.product).toBeDefined()
  expect(resp.data.product.name).toBe(data.name)
  expect(resp.data.product.cost).toBe(data.cost)
  expect(resp.data.product.amountAvailable).toBe(data.amountAvailable)
  expect(resp.data.product._id).toBeDefined()

  let prod = await Product.findById(resp.data.product._id)
  expect(prod.name).toBe(data.name)
  expect(prod.cost).toBe(data.cost)
  expect(prod.amountAvailable).toBe(data.amountAvailable)

  let creatorUser = await User.findById(prod.userId)
  expect(creatorUser.name).toBe(seller.name)
});

test('add new product - no body', async () => {
  let data = {
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('add new product - name empty', async () => {
  let data = {
    cost: 10,
    amountAvailable: 10
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('add new product - name length 0', async () => {
  let data = {
    name: '',
    cost: 10,
    amountAvailable: 10
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('add new product - cost empty', async () => {
  let data = {
    name: 'product' + new Date().getTime(),
    amountAvailable: 10
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('add new product - amountAvailable empty', async () => {
  let data = {
    name: 'product' + new Date().getTime(),
    cost: 1000,
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('add new product - cost lower than 0', async () => {
  let data = {
    name: 'product' + new Date().getTime(),
    cost: -1,
    amountAvailable: 10
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('add new product - cost not dividable by 5', async () => {
  let data = {
    name: 'product' + new Date().getTime(),
    cost: 4,
    amountAvailable: 10
  }
  let config = getHeader(sellerToken)

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('add new product - same product name', async () => {
  let data: any = {
    name: '',
    cost: 100,
    amountAvailable: 10
  }
  let config = getHeader(sellerToken)

  let anyProduct = await Product.findOne()
  data.name = anyProduct.name

  let resp = await axios.post(getApiRoute("product"), data, config)
  expect(resp.data.success).toBe(false)
});

test('put product - change name', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })
  let anyProduct = await Product.findOne({
    userId: user._id
  })
  let data: any = {
    name: 'newProductName' + new Date().getTime(),
    amountAvailable: anyProduct.amountAvailable,
    cost: anyProduct.cost
  }
  let route = getApiRoute("product") + "/" + anyProduct._id

  let resp = await axios.put(route, data, config)
  expect(resp.data.success).toBe(true)
  let changedProduct = await Product.findOne({
    _id: anyProduct._id
  })
  expect(changedProduct.name).toBe(data.name)
  expect(changedProduct.cost).toBe(anyProduct.cost)
  expect(changedProduct.amountAvailable).toBe(anyProduct.amountAvailable)
});

test('put product - change cost', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })
  let anyProduct = await Product.findOne({
    userId: user._id
  })
  let data: any = {
    name: anyProduct.name,
    amountAvailable: anyProduct.amountAvailable,
    cost: anyProduct.cost + 10
  }
  let route = getApiRoute("product") + "/" + anyProduct._id

  let resp = await axios.put(route, data, config)
  expect(resp.data.success).toBe(true)
  let changedProduct = await Product.findOne({
    _id: anyProduct._id
  })
  expect(changedProduct.name).toBe(anyProduct.name)
  expect(changedProduct.cost).toBe(data.cost)
  expect(changedProduct.amountAvailable).toBe(anyProduct.amountAvailable)
});

test('put product - change amountAvailable', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })
  let anyProduct = await Product.findOne({
    userId: user._id
  })
  let data: any = {
    name: anyProduct.name,
    amountAvailable: anyProduct.amountAvailable + 1,
    cost: anyProduct.cost
  }
  let route = getApiRoute("product") + "/" + anyProduct._id

  let resp = await axios.put(route, data, config)
  expect(resp.data.success).toBe(true)
  let changedProduct = await Product.findOne({
    _id: anyProduct._id
  })
  expect(changedProduct.name).toBe(anyProduct.name)
  expect(changedProduct.cost).toBe(anyProduct.cost)
  expect(changedProduct.amountAvailable).toBe(data.amountAvailable)
});

test('put product - cost < 0', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })
  let anyProduct = await Product.findOne({
    userId: user._id
  })
  let data: any = {
    name: anyProduct.name,
    amountAvailable: anyProduct.amountAvailable + 1,
    cost: -1
  }
  let route = getApiRoute("product") + "/" + anyProduct._id

  let resp = await axios.put(route, data, config)
  expect(resp.data.success).toBe(false)
  let changedProduct = await Product.findOne({
    _id: anyProduct._id
  })
  expect(changedProduct.name).toBe(anyProduct.name)
  expect(changedProduct.cost).toBe(anyProduct.cost)
  expect(changedProduct.amountAvailable).toBe(anyProduct.amountAvailable)
});

test('put product - cost % 5 != 0', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })
  let anyProduct = await Product.findOne({
    userId: user._id
  })
  let data: any = {
    name: anyProduct.name,
    amountAvailable: anyProduct.amountAvailable + 1,
    cost: 16
  }
  let route = getApiRoute("product") + "/" + anyProduct._id

  let resp = await axios.put(route, data, config)
  expect(resp.data.success).toBe(false)
  let changedProduct = await Product.findOne({
    _id: anyProduct._id
  })
  expect(changedProduct.name).toBe(anyProduct.name)
  expect(changedProduct.cost).toBe(anyProduct.cost)
  expect(changedProduct.amountAvailable).toBe(anyProduct.amountAvailable)
});

test('put product - of another user', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })
  let anyProduct = await Product.findOne({
    userId: { $ne: user._id }
  })
  let data: any = {
    name: anyProduct.name,
    amountAvailable: anyProduct.amountAvailable + 1,
    cost: 10
  }
  let route = getApiRoute("product") + "/" + anyProduct._id

  let resp = await axios.put(route, data, config)
  expect(resp.data.success).toBe(false)
  let changedProduct = await Product.findOne({
    _id: anyProduct._id
  })
  expect(changedProduct.name).toBe(anyProduct.name)
  expect(changedProduct.cost).toBe(anyProduct.cost)
  expect(changedProduct.amountAvailable).toBe(anyProduct.amountAvailable)
});

test('delete product', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })

  let route = getApiRoute("product")
  let anyProduct = await Product.findOne({
    userId: user._id
  })
  route += "/" + anyProduct._id

  let resp = await axios.delete(route, config)
  expect(resp.data.success).toBe(true)
  expect(await Product.findById(anyProduct._id)).toBeNull()
});

test('delete product - of another user', async () => {
  let config = getHeader(sellerToken)

  let user = await User.findOne({
    name: seller.name
  })

  let route = getApiRoute("product")
  let anyProduct = await Product.findOne({
    userId: { $ne: user._id }
  })
  route += "/" + anyProduct._id

  let resp = await axios.delete(route, config)
  expect(resp.data.success).toBe(false)
  expect(await Product.findById(anyProduct._id)).not.toBeNull()
});
