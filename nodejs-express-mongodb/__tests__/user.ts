import axios, { AxiosError } from 'axios'
import mongoose from "mongoose";
import { dbHost } from "../src/config";
import { url, getApiRoute, getHeader } from "./services/service"
// DB models
import { User } from '../src/models/user'
import { Product } from '../src/models/product'


let buyer = {
  name: "buyer",
  password: "test"
}
let buyerToken: string = null

// before and after
beforeAll(() => {
  console.log("user - before all")

  return new Promise<void>((resolve, reject) => {
    mongoose.connect(dbHost, {
      autoIndex: true,
    }, (result) => {
      console.log("connected to mongoose")

      let data = buyer
      axios.post(getApiRoute("login"), data)
        .then(resp => {
          buyerToken = resp.data.token
          resolve()
        })
    })
  });
});

afterAll(() => {
  console.log("user - after all")

  mongoose.disconnect()
});


//////////////
// tests
////

// deposit test
test('deposit test', async () => {
  console.log('deposit test');

  let data: any = {
    "five": 1,
    "ten": 0,
    "twenty": 0,
    "fifty": 0,
    "hundret": 1
  }
  let depositNumber = 105

  let user = await User.findOne({
    name: buyer.name
  })
  let depositBeforeAdd = user.deposit

  let route = getApiRoute("deposit")

  let config = getHeader(buyerToken)
  let resp = await axios.post(route, data, config)
  expect(resp.data.success).toBe(true)
  expect(resp.data.add).toBe(depositNumber)
  expect(resp.data.deposit).toBe(depositBeforeAdd + depositNumber)

  let userAfterSave = await User.findOne({
    name: buyer.name
  })

  expect(userAfterSave.deposit).toBe(depositBeforeAdd + depositNumber)
});

test('deposit test - negative value', async () => {
  console.log('deposit test - negative value');

  let data: any = {
    "five": 0,
    "ten": 0,
    "twenty": 0,
    "fifty": 0,
    "hundret": -1
  }

  let user = await User.findOne({
    name: buyer.name
  })

  let route = getApiRoute("deposit")

  let config = getHeader(buyerToken)
  let resp = await axios.post(route, data, config)
  expect(resp.data.success).toBe(false)

  let userAfterSave = await User.findOne({
    name: buyer.name
  })
  expect(userAfterSave.deposit).toBe(user.deposit)
});


// buy test
test('buy test - single product', async () => {
  console.log('buy test - single product');

  let productToBuy = await Product.findOne({
    amountAvailable: {
      $gte: 1
    }
  })
  let user = await User.findOne({
    name: buyer.name
  })

  // make sure enough deposit is available
  let amountOfHundertsToAdd = Math.ceil(productToBuy.cost / 100)
  let dataDepo = {
    "five": 0,
    "ten": 0,
    "twenty": 0,
    "fifty": 0,
    "hundret": amountOfHundertsToAdd
  }
  let config = getHeader(buyerToken)
  let respDepo = await axios.post(getApiRoute("deposit"), dataDepo, config)
  expect(respDepo.data.success).toBe(true)

  // buy product
  let data = {
    products: [
      {
        id: productToBuy.id,
        quantity: 1
      }
    ]
  }
  let resp = await axios.post(getApiRoute("buy"), data, config)
  expect(resp.data.success).toBe(true)
  expect(resp.data.spent).toBe(productToBuy.cost)

  expect(resp.data.change).toBeDefined()
  expect(resp.data.change.five).toBeDefined()
  expect(resp.data.change.ten).toBeDefined()
  expect(resp.data.change.twenty).toBeDefined()
  expect(resp.data.change.fifty).toBeDefined()
  expect(resp.data.change.hundert).toBeDefined()

  let userDepositBeforeBuy = user.deposit + amountOfHundertsToAdd * 100
  expect(resp.data.deposit.before).toBe(userDepositBeforeBuy)
  expect(resp.data.deposit.now).toBe(userDepositBeforeBuy - productToBuy.cost)
});

test('buy test - not enough deposit', async () => {
  console.log('buy test - not enough deposit');

  let productToBuy = await Product.findOne({
    amountAvailable: {
      $gte: 1
    }
  })
  let user = await User.findOne({
    name: buyer.name
  })

  // make sure enough deposit is available
  let config = getHeader(buyerToken)
  let respDepo = await axios.post(getApiRoute("reset"), {}, config)
  expect(respDepo.data.success).toBe(true)

  // buy product
  let data = {
    products: [
      {
        id: productToBuy.id,
        quantity: 1
      }
    ]
  }
  let resp = await axios.post(getApiRoute("buy"), data, config)
  expect(resp.data.success).toBe(false)

  let userAfterBuy = await User.findOne({
    name: buyer.name
  })
  expect(userAfterBuy.deposit).toBe(0) // was reseted
});

test('buy test - negative quantity', async () => {
  console.log('buy test - negative quantity');

  let productToBuy = await Product.findOne({
    amountAvailable: {
      $gte: 1
    }
  })
  let user = await User.findOne({
    name: buyer.name
  })

  // make sure enough deposit is available
  let config = getHeader(buyerToken)
  let respDepo = await axios.post(getApiRoute("reset"), {}, config)
  expect(respDepo.data.success).toBe(true)

  // buy product
  let data = {
    products: [
      {
        id: productToBuy.id,
        quantity: -1
      }
    ]
  }
  let resp = await axios.post(getApiRoute("buy"), data, config)
  expect(resp.data.success).toBe(false)

  let userAfterBuy = await User.findOne({
    name: buyer.name
  })
  expect(userAfterBuy.deposit).toBe(0) // was reseted
});

test('buy test - not enough products available', async () => {
  console.log('buy test - not enough products available');

  let productToBuy = await Product.findOne({
    amountAvailable: 0
  })
  let user = await User.findOne({
    name: buyer.name
  })

  // buy product
  let data = {
    products: [
      {
        id: productToBuy.id,
        quantity: 1
      }
    ]
  }
  let config = getHeader(buyerToken)
  let resp = await axios.post(getApiRoute("buy"), data, config)
  expect(resp.data.success).toBe(false)

  let userAfterBuy = await User.findOne({
    name: buyer.name
  })
  expect(userAfterBuy.deposit).toBe(userAfterBuy.deposit)
});
