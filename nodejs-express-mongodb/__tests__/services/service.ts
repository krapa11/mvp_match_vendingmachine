const url = "http://localhost:8080"

function getApiRoute(subroute: string): string {
  return url + "/api/" + subroute
}

function getHeader(token: string): any {
  const config = {
    headers: {
      Authorization: 'Bearer ' + token
    }
  }
  return config
}


export {
  url,
  getApiRoute,
  getHeader,
}