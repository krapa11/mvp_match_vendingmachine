import mongoose from "mongoose";
import bcrypt from "bcrypt";
import { refreshTokenValidTimeInSec } from "../config";
import { v5 as uuidv5, v4 as uuidv4 } from 'uuid';

const SALT_FACTOR = 10;


// interfaces
interface IRefreshToken {
  validUntil: number;
  token: string;
}
interface IUser {
  name: string;
  password: string;
  deposit: number;
  role: string;
  refreshToken: IRefreshToken;
  logoutAllSince: number;
}
interface UserDoc extends mongoose.Document {
  name: string;
  password: string;
  deposit: number;
  role: string;
  refreshToken: IRefreshToken;
  logoutAllSince: number;

  isBuyer(): Boolean;
  isSeller(): Boolean;
  createNewRefreshToken(): IRefreshToken;
  savedRefreshTokenIsValid(): Promise<boolean>;
  refreshTokenIsValid(refreshToken: string): Promise<boolean>;
}
interface UserModelInterface extends mongoose.Model<UserDoc> {
  build(attr: IUser): UserDoc
  getUserFromToken(token: any): Promise<UserDoc>
}


// schema
const userSchema = new mongoose.Schema({
  name: {
    type: String,
    index: true,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  deposit: {
    type: Number,
    required: true,
    default: 0
  },
  role: {
    type: String,
    required: true
  },
  refreshToken: {
    validUntil: {
      type: Number
    },
    token: {
      type: String
    }
  },
  logoutAllSince: {
    required: false,
    type: Number
  }
})


// pre save
function getHashOfString(password: string): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
      if (err) {
        throw 'Error';
      }

      bcrypt.hash(password, salt, function (err, hash) {
        if (err) {
          throw 'Error';
        }

        resolve(hash);
      });
    });
  });
}

function confirmPassword(password: string, hash: string): Promise<boolean> {
  return bcrypt.compare(password, hash)
}

// password
userSchema.pre('save', function (next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();
  // generate a salt
  getHashOfString(user.password)
    .then(hash => {
      user.password = hash;
      next();
    })
    .catch(err => {
      next(err);
    })
});

// refresh token
userSchema.pre('save', function (next) {
  var user = this;

  if (!user.isModified('refreshToken')) return next();
  // generate a salt
  getHashOfString(user.refreshToken.token)
    .then(hash => {
      user.refreshToken.token = hash;
      next();
    })
    .catch(err => {
      next(err);
    })
});


// document functions
userSchema.statics.build = (attr: IUser) => {
  return new User(attr)
}
userSchema.statics.getUserFromToken = (token: any) => {
  let userId = token.sub

  return User.findOne({ _id: userId })
}

userSchema.methods.comparePassword = function(candidatePassword: string, cb: any) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};
userSchema.methods.isBuyer = function(): Boolean {
  return this.role == "buyer"
};
userSchema.methods.isSeller = function (): Boolean {
  return this.role == "seller"
};
userSchema.methods.createNewRefreshToken = function (): IRefreshToken {
  let nowInSecs = new Date().getTime() / 1000
  let validUntil = nowInSecs + refreshTokenValidTimeInSec

  let namespace = uuidv4()
  let token = uuidv5(this.name, namespace)

  let refreshToken: IRefreshToken = {
    validUntil: validUntil,
    token: token
  }

  return refreshToken
};
userSchema.methods.savedRefreshTokenIsValid = async function (): Promise<boolean> {
  if (this.refreshToken?.token == null) {
    return false
  }

  // check valid until date of refresh token
  let now = new Date().getTime() / 1000
  if (this.refreshToken.validUntil < now) {
    return false
  }

  return true
}
userSchema.methods.refreshTokenIsValid = async function (refreshToken: string): Promise<boolean> {
  if (this.savedRefreshTokenIsValid() == false) {
    return false
  }

  // check if token matches
  try {
    let tokenIsCorrect = await bcrypt.compare(refreshToken, this.refreshToken.token)
    if (tokenIsCorrect) {
      return true
    }
  } catch (error) {
    return false
  }

  return false
};


// export
const User = mongoose.model<UserDoc, UserModelInterface>('User', userSchema)

export {
  User,
  confirmPassword
}