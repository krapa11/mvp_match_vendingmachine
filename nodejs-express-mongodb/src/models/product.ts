import mongoose from "mongoose";


// interfaces
interface IProduct {
  amountAvailable: number;
  cost: number;
  name: String;
  userId: mongoose.Types.ObjectId;
}
interface ProductDoc extends mongoose.Document {
  amountAvailable: number;
  cost: number;
  name: String;
  userId: mongoose.Types.ObjectId;
}
interface ProductModelInterface extends mongoose.Model<ProductDoc> {
  build(attr: IProduct): ProductDoc
}


// schema
const productSchema = new mongoose.Schema({
  amountAvailable: {
    type: Number,
    required: true
  },
  cost: {
    type: Number,
    required: true,
    min: 0
  },
  name: {
    type: String,
    index: true,
    unique: true,
    required: true,
  },
  userId: {
    type: mongoose.Types.ObjectId,
    required: true
  }
})



// document functions
productSchema.statics.build = (attr: IProduct) => {
  return new Product(attr)
}


// export
const Product = mongoose.model<ProductDoc, ProductModelInterface>('Product', productSchema)

export {
  Product
}