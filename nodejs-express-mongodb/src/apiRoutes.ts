import express from 'express'
// routes
import register from './routes/auth/register'
import login from './routes/auth/login'
import loginRefreshToken from './routes/auth/loginRefreshToken'
import logout from './routes/auth/logout'
import logoutAll from './routes/auth/logoutAll'
import dashboard from './routes/dashboard'
import addDeposit from './routes/deposit/addDeposit'
import resetDeposit from './routes/deposit/resetDeposit'
import addProduct from './routes/product/addProduct'
import editProduct from './routes/product/editProduct'
import deleteProduct from './routes/product/deleteProduct'
import getProducts from './routes/product/getProducts'
import buyProducts from './routes/product/buyProducts'
import getUser from './routes/user/getUser'
import editUser from './routes/user/editUser'
// models
import { User } from "./models/User";


let apiRouter = express.Router()


// unauthenticated
apiRouter.route('/register')
  .post(register)

apiRouter.route("/login")
  .post(login);

apiRouter.route("/logout/all")
  .post(logoutAll);

apiRouter.route("/refresh-token")
  .post(loginRefreshToken);

apiRouter.route("/product/:id?")
  .get(getProducts)


// authenticated
apiRouter.use(async (req, res, next) => {
  console.log("request")
  console.log(req.user);

  // get user
  let token = (req as any).user
  try {
    let user = await User.getUserFromToken(token)
    if (user == null) {
      throw "User not found"
    }

    // check if logout all since is set
    let tokenIssuedAt = token.iat * 1000
    if (tokenIssuedAt < user.logoutAllSince) {
      // token is too old
      res.statusCode = 401
      return res.json({
        error: "Unauthorized"
      })
    }

    (req as any).userObj = user
    next()
  }
  catch(error) {
    return res.json({
      success: false
    })
  }
})

apiRouter.route("/logout")
  .post(logout);

apiRouter.route("/user")
  .get(getUser)
  .put(editUser)

apiRouter.route("/dashboard")
  .get(dashboard)

apiRouter.route("/deposit")
  .post(addDeposit)

apiRouter.route("/reset")
  .post(resetDeposit)

apiRouter.route("/buy")
  .post(buyProducts)

apiRouter.route("/product")
  .post(addProduct)

apiRouter.route("/product/:id")
  .put(editProduct)
  .delete(deleteProduct)


export default apiRouter;