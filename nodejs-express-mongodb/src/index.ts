import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from "mongoose";
import expressJwt from "express-jwt";
import { secret, jwtAlgorithm, dbHost } from "./config";
import apiRouter from "./apiRoutes";


// database connect
mongoose.connect(dbHost, {
  autoIndex: true,
}, (result) => {
  console.log("connect to mongoose", result)
})

// express
const app = express();
const port = 8080; // default port to listen
const corsOptions = {
  origin: "http://localhost:8081"
};

// middleware
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', expressJwt({
  secret: secret,
  algorithms: [jwtAlgorithm]
}).unless({ path: [
  '/api/register',
  '/api/login',
  '/api/refresh-token',
  '/api/logout/all',
  { url: new RegExp("\/api\/product[\/.+]?"), methods: ['GET'] },
] }));
app.use(express.static('../vue3/dist'))

// define a route handler for the default home page
app.get("/", async (req, res) => {
  return res.json({ message: "Welcome to your application." });
});

// api routes
app.use("/api", apiRouter)


// start the express server
app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`server started at http://localhost:${port}`);
});