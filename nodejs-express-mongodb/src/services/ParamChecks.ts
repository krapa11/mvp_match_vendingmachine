export function checkStringParam(string: any, required: boolean = true) {
  if (required == false) {
    if (string != null && typeof string !== "string") {
      throw "false params"
    }
  } else {
    if (string == null) {
      throw "missing params"
    }
    if (typeof string !== "string") {
      throw "false params"
    }
  }
}

export function checkBoolParam(bool: any, required: boolean = true) {
  if (required == false) {
    if (bool != null && typeof bool !== "boolean") {
      throw "false params"
    }
  } else {
    if (bool == null) {
      throw "missing params"
    }
    if (typeof bool !== "boolean") {
      throw "false params"
    }
  }
}

export function checkNumberParam(number: any, required: boolean = true) {
  if (required == false) {
    if (number != null && typeof number !== "number") {
      throw "false params"
    }
  } else {
    if (number == null) {
      throw "missing params"
    }
    if (typeof number !== "number") {
      throw "false params"
    }
  }
}