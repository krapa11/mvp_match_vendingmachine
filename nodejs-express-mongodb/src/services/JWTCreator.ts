import jwt from "jsonwebtoken";
import { secret, jwtAlgorithm, tokenValidTimeInSec, refreshTokenValidTimeInSec } from "../config"

class JWTCreator {

  createNewAuthToken(user: any): string {
    var token = jwt.sign({
      name: user.name,
      role: user.role,
    }, secret, {
      algorithm: jwtAlgorithm,
      expiresIn: tokenValidTimeInSec,
      subject: String(user._id)
    });

    return token
  }

}

const jwtCreator = new JWTCreator()
export default jwtCreator