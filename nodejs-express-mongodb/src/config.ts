const secret = '8PvA?e6+F_Lc}haK:9J#'
const jwtAlgorithm = 'HS256'
const dbHost = 'mongodb://127.0.0.1:27017/mvp'

const tokenValidTimeInSec = 60 * 5 // 5 min
const refreshTokenValidTimeInSec = 60 * 60 * 24 // 1 day

export {
  secret,
  jwtAlgorithm,
  dbHost,
  tokenValidTimeInSec,
  refreshTokenValidTimeInSec,
}