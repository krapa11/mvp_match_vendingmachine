import {
  checkStringParam
} from '../../services/ParamChecks'
import { User, confirmPassword } from "../../models/User";
import { Request, Response } from "express"
import { refreshTokenValidTimeInSec } from "../../config";
import jwt from "jsonwebtoken";
import JWTCreator from "../../services/JWTCreator"


export default async function login (req: Request, res: Response) {
  let name = req.body.name
  let password = req.body.password

  // check for params
  try {
    checkStringParam(name)
    checkStringParam(password)
  } catch (err) {
    return res.json({
      success: false,
      error: err
    })
  }

  // check credentials
  const incorrectCredentialsResp = {
    success: false,
    error: "Invalid Login Credentials"
  }

  // get user by username
  let user = await User.findOne({ name: name })
  if (user == null) {
    res.json(incorrectCredentialsResp)
    return
  }

  try {
    let passwordIsCorrect = await confirmPassword(
      password,
      user.password
    )

    if (passwordIsCorrect) {
      // correct credentials
      const token = JWTCreator.createNewAuthToken(user)

      // check if valid refresh token exists for user
      let alreadyLoggedIn = await user.savedRefreshTokenIsValid()

      // set new refresh token
      let refreshToken = user.createNewRefreshToken();
      user.refreshToken = refreshToken
      await user.save()

      res.cookie('refresh-token', refreshToken.token, {
        maxAge: refreshTokenValidTimeInSec * 1000
      })

      res.json({
        success: true,
        token: token,
        alreadyLoggedIn: alreadyLoggedIn,
        refreshToken: refreshToken.token
      })
    } else {
      // incorrect credentials
      res.json(incorrectCredentialsResp)
    }
  } catch (err) {
    res.json({
      success: false,
      error: err
    })
  }
}