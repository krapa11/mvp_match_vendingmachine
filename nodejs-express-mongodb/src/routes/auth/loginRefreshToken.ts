import {
  checkStringParam
} from '../../services/ParamChecks'
import { User, confirmPassword } from "../../models/User";
import { Request, Response } from "express"
import jwt from "jsonwebtoken";
import JWTCreator from "../../services/JWTCreator"

function getPayloadOfAccessToken(accessToken: string): any {
  accessToken = accessToken.replace("Bearer ", "")
  let decodedToken = jwt.decode(accessToken)

  return decodedToken
}

export default async function loginRefreshToken (req: Request, res: Response) {
  try {
    let accessToken = req.headers.authorization
    if (accessToken == null) {
      throw "No Access Token set"
    }

    let decodedToken = getPayloadOfAccessToken(accessToken)

    let user = await User.findById(decodedToken.sub)
    if (user == null) {
      throw "User not found"
    }

    let refreshToken = req.body.refreshToken
    // check for params
    checkStringParam(refreshToken)

    let refreshTokenIsValid = await user.refreshTokenIsValid(refreshToken)
    if (refreshTokenIsValid == false) {
      throw "Token is not valid"
    }

    // create new auth token
    const token = JWTCreator.createNewAuthToken(user)

    res.json({
      success: true,
      token: token
    })
  } catch (err) {
    res.json({
      success: false,
      error: err
    })
  }
}