import { User } from "../../models/User";
import { Request, Response } from "express"


export default async function logout (req: Request, res: Response) {
  try {
    let user = (req as any).userObj
    if (user == null) {
      throw "User not found"
    }

    await User.updateOne({_id: user._id}, {
      $unset: {
        refreshToken: 1
      }
    })

    return res.json({
      success: true
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}