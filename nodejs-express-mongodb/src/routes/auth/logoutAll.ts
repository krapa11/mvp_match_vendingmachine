import {
  checkStringParam
} from '../../services/ParamChecks'
import { User, confirmPassword } from "../../models/User";
import { Request, Response } from "express"


export default async function logoutAll (req: Request, res: Response) {
  let name = req.body.name
  let password = req.body.password

  // check for params
  try {
    checkStringParam(name)
    checkStringParam(password)

    // check credentials
    const incorrectCredentialsResp = {
      success: false,
      error: "Invalid Login Credentials"
    }

    // get user by username
    let user = await User.findOne({ name: name })
    if (user == null) {
      res.json(incorrectCredentialsResp)
      return
    }

    let passwordIsCorrect = await confirmPassword(
      password,
      user.password
    )

    if (passwordIsCorrect) {
      // correct credentials
      await User.updateOne({ _id: user._id }, {
        $set: {
          logoutAllSince: new Date().getTime()
        },
        $unset: {
          refreshToken: 1
        }
      })

      return res.json({
        success: true
      })
    } else {
      // incorrect credentials
      res.json(incorrectCredentialsResp)
    }
  } catch (err) {
    res.json({
      success: false,
      error: err
    })
  }
}