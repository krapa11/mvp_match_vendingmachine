import {
  checkStringParam,
  checkBoolParam
} from '../../services/ParamChecks'
import { User } from "../../models/User";
import { Request, Response } from "express"

export default function register(req: Request, res: Response) {
  let name = req.body.name
  let password = req.body.password
  let isSeller = req.body.isSeller

  // check for params
  try {
    checkStringParam(name)
    checkStringParam(password)
    checkBoolParam(isSeller)
  } catch (err) {
    return res.json({
      success: false,
      error: err
    })
  }

  // save user
  let role = "buyer"
  if (isSeller) {
    role = "seller"
  }

  let user = User.build({
    name: name as string,
    password: password as string,
    deposit: 0,
    role: role,
    refreshToken: null,
    logoutAllSince: null
  })
  console.log("create user", user)

  user.save()
    .then(docSaved => {
      if (user === docSaved) {
        // success
        return res.json({
          success: true,
          message: "User " + docSaved.name + " (" + docSaved.role + ")" + " created"
        });
      } else {
        // error
        return res.json({
          success: false,
          error: "Error while saving"
        });
      }
    })
    .catch(error => {
      console.log(error)
      if (error.code === 11000) {
        return res.json({
          success: false,
          error: "Name already taken"
        });
      } else {
        return res.json({
          success: false,
          error: "Error while saving"
        });
      }
    })
}