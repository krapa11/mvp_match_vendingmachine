import { Request, Response } from "express"
import { User } from "../models/User";


export default async function dashboard(req: Request, res: Response) {
  try {
    let user = (req as any).userObj

    res.json({
      success: true,
      user: {
        _id: user._id,
        name: user.name,
        deposit: user.deposit,
        role: user.role
      }
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}