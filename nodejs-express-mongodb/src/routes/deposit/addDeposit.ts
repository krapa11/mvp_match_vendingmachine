import {
  checkNumberParam
} from '../../services/ParamChecks'
import { Request, Response } from "express"
import { User } from "../../models/User";


export default async function addDeposit(req: Request, res: Response) {
  try {
    let user = (req as any).userObj
    if (user.isBuyer() == false) {
      res.json({
        success: false,
        error: "only buyers can add deposit"
      })
      return
    }

    let five = req.body.five
    let ten = req.body.ten
    let twenty = req.body.twenty
    let fifty = req.body.fifty
    let hundret = req.body.hundret

    // check type of params
    try {
      checkNumberParam(five)
      checkNumberParam(ten)
      checkNumberParam(twenty)
      checkNumberParam(fifty)
      checkNumberParam(hundret)

      if (
        five < 0 ||
        ten < 0 ||
        twenty < 0 ||
        fifty < 0 ||
        hundret < 0
      ) {
        throw "Values have to be greater than 0"
      }
    } catch (err) {
      res.json({
        success: false,
        error: err
      })
      return
    }

    let addedDeposit = 0
    if (five != null) {
      addedDeposit += 5 * five
    }
    if (ten != null) {
      addedDeposit += 10 * ten
    }
    if (twenty != null) {
      addedDeposit += 20 * twenty
    }
    if (fifty != null) {
      addedDeposit += 50 * fifty
    }
    if (hundret != null) {
      addedDeposit += 100 * hundret
    }


    user.deposit = user.deposit + addedDeposit
    await user.save()

    res.json({
      success: true,
      add: addedDeposit,
      deposit: user.deposit
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}