import { Request, Response } from "express"
import { User } from "../../models/User";


export default async function resetDeposit(req: Request, res: Response) {
  try {
    let user = (req as any).userObj
    if (user.isBuyer() == false) {
      res.json({
        success: false,
        error: "only buyers can add deposit"
      })
      return
    }

    let prevDeposit = user.deposit
    user.deposit = 0
    await user.save()

    res.json({
      success: true,
      prevDeposit: prevDeposit,
      deposit: user.deposit
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}