import { Request, Response } from "express"
import { User } from "../../models/User";
import {
  checkStringParam,
} from '../../services/ParamChecks'


export default async function editUser(req: Request, res: Response) {
  try {
    let user = (req as any).userObj

    let name = req.body.name
    let password = req.body.password

    // check for params
    checkStringParam(name, false)
    checkStringParam(password, false)

    if (name) {
      // check if name exists already
      let userWithSameName = await User.findOne({name: name})
      if (userWithSameName != null && userWithSameName.id != user.id) {
        throw "Username exists already"
      }
      user.name = name
    }
    if (password) {
      user.password = password
    }
    await user.save()

    res.json({
      success: true,
      user: {
        name: user.name
      }
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}