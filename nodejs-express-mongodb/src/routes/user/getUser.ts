import { Request, Response } from "express"
import { User } from "../../models/User";


export default async function getUser(req: Request, res: Response) {
  try {
    let user = (req as any).userObj

    let userInBody = user

    res.json({
      success: true,
      user: {
        name: userInBody.name,
        role: userInBody.role
      }
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}