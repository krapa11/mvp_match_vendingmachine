import {
  checkNumberParam,
  checkStringParam
} from '../../services/ParamChecks'
import { Request, Response } from "express"
import { User } from "../../models/User";
import { Product } from "../../models/Product";


interface IChange {
  five: number;
  ten: number;
  twenty: number;
  fifty: number;
  hundert: number;
}

function calculateChange(missingChange: number): IChange {
  let change: IChange = {
    five: 0,
    ten: 0,
    twenty: 0,
    fifty: 0,
    hundert: 0
  }

  let divHundert = missingChange / 100
  if (divHundert >= 1) {
    change.hundert = Math.floor(divHundert)
  }
  missingChange -= change.hundert * 100

  let divFifty = missingChange / 50
  if (divFifty >= 1) {
    change.fifty = Math.floor(divFifty)
  }
  missingChange -= change.fifty * 50

  let divTwenty = missingChange / 20
  if (divTwenty >= 1) {
    change.twenty = Math.floor(divTwenty)
  }
  missingChange -= change.twenty * 20

  let divTen = missingChange / 10
  if (divTen >= 1) {
    change.ten = Math.floor(divTen)
  }
  missingChange -= change.ten * 10

  let divFive = missingChange / 5
  if (divFive >= 1) {
    change.five = Math.floor(divFive)
  }
  missingChange -= change.five * 5

  if (missingChange > 0) {
    throw "Cannot give charge"
  }

  return change
}

export default async function buyProducts(req: Request, res: Response) {
  try {
    let user = (req as any).userObj
    if (user.isBuyer() == false) {
      res.json({
        success: false,
        error: "only buyers can buy a product"
      })
      return
    }

    let reqBody: {
      id: string,
      quantity: number,
      product?: any | null
    }[] = req.body.products
    // check type of params
    try {
      reqBody.forEach((element: any) => {
        checkStringParam(element.id)
        checkNumberParam(element.quantity)
      });
    } catch (err) {
      res.json({
        success: false,
        error: err
      })
      return
    }

    // get products
    let ids: string[] = []
    ids = reqBody.map(item => item.id)
    let productsFound = await Product.find().where("_id").in(ids).exec()

    let totalPrice = 0
    reqBody.forEach((element: any) => {
      let productId = element.id
      let quantity = element.quantity

      let product = productsFound.find(product => product._id == productId)
      if (product == null) {
        throw "Product not found"
      }

      // check if enough products available
      let productsAvailable = product.amountAvailable
      if (quantity < 0) {
        throw "Quantity has to be greater than 0"
      }
      if ((productsAvailable >= quantity) === false) {
        throw "Not enough Products available"
      }

      totalPrice += product.cost * quantity

      element.product = product
    })


    // check if user has enough deposit
    let userDeposit = user.deposit
    if (userDeposit < totalPrice) {
      // not liquid enough
      return res.json({
        success: false,
        error: "User has not enough Deposit"
      })
    }

    // decrease product amount and user deposit
    reqBody.forEach(async (element: any) => {
      element.product.amountAvailable -= element.quantity
      await element.product.save()
    })
    user.deposit -= totalPrice
    await user.save()

    // get purchased products
    let productsPurchased = reqBody.filter(element => {
      if (element.quantity > 0) {
        return true
      }
      return false
    })
    productsPurchased = productsPurchased.map(element => {
      return element.product
    })

    // calculate change
    let change: IChange = calculateChange(user.deposit)

    res.json({
      success: true,
      spent: totalPrice,
      change: change,
      products: productsPurchased,
      deposit: {
        before: userDeposit,
        now: user.deposit
      }
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}