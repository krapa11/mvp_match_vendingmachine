import {
  checkNumberParam,
  checkStringParam
} from '../../services/ParamChecks'
import { Request, Response } from "express"
import { User } from "../../models/User";
import { Product } from "../../models/Product";


export default async function editProduct(req: Request, res: Response) {
  try {
    let user = (req as any).userObj
    if (user.isSeller() == false) {
      res.json({
        success: false,
        error: "only sellers can add products"
      })
      return
    }

    // get params
    let productId = req.params.id
    let amountAvailable = req.body.amountAvailable
    let cost = req.body.cost
    let name = req.body.name
    // check for params
    try {
      checkStringParam(productId)
      checkNumberParam(amountAvailable, false)
      checkNumberParam(cost, false)
      checkStringParam(name, false)
    } catch (err) {
      return res.json({
        success: false,
        error: err
      })
    }

    // cost has to be divideable by 5
    if (cost % 5 !== 0) {
      return res.json({
        success: false,
        error: "Product Cost has to be divideable by 5"
      })
    }

    // get product
    let product = await Product.findOne({ _id: productId })
    if (product == null) {
      res.json({
        success: false,
        error: "product does not exist"
      })
      return
    }

    // check if user owns product
    if (product.userId.toString() !== user.id) {
      throw "Product is owned by another user"
    }

    // override product
    if (amountAvailable != null) {
      product.amountAvailable = amountAvailable
    }
    if (cost != null) {
      product.cost = cost
    }
    if (name != null) {
      product.name = name
    }

    await product.save()

    res.json({
      success: true,
      product: product
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}