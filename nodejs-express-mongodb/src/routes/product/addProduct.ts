import {
  checkNumberParam,
  checkStringParam
} from '../../services/ParamChecks'
import { Request, Response } from "express"
import { User } from "../../models/User";
import { Product } from "../../models/Product";


export default async function addProduct(req: Request, res: Response) {
  try {
    let user = (req as any).userObj
    if (user.isSeller() == false) {
      res.json({
        success: false,
        error: "only sellers can add products"
      })
      return
    }

    let amountAvailable = req.body.amountAvailable
    let cost = req.body.cost
    let name = req.body.name

    // check for params
    try {
      checkNumberParam(amountAvailable)
      checkNumberParam(cost)
      checkStringParam(name)
    } catch (err) {
      return res.json({
        success: false,
        error: err
      })
    }

    // cost has to be divideable by 5
    if (cost % 5 !== 0) {
      return res.json({
        success: false,
        error: "Product Cost has to be divideable by 5"
      })
    }

    // add product
    let product = Product.build({
      amountAvailable: amountAvailable,
      cost: cost,
      name: name,
      userId: user._id
    })
    await product.save()

    res.json({
      success: true,
      product: product
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}