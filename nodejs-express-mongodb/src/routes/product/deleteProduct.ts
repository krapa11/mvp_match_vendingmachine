import {
  checkStringParam
} from '../../services/ParamChecks'
import { Request, Response } from "express"
import { User } from "../../models/User";
import { Product } from "../../models/Product";


export default async function deleteProduct(req: Request, res: Response) {
  try {
    let user = (req as any).userObj
    if (user.isSeller() == false) {
      res.json({
        success: false,
        error: "only sellers can add products"
      })
      return
    }

    let productId = req.params.id
    // check for params
    try {
      checkStringParam(productId)
    } catch (err) {
      return res.json({
        success: false,
        error: err
      })
    }

    // get product
    let product = await Product.findOne({ _id: productId })
    if (product == null) {
      res.json({
        success: false,
        error: "product does not exist"
      })
      return
    }

    // check if user own product
    if (String(product.userId) != String(user._id)) {
      res.json({
        success: false,
        error: "user does not own product"
      })
      return
    }

    // delete product
    await Product.deleteOne({ _id: productId })

    res.json({
      success: true
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}