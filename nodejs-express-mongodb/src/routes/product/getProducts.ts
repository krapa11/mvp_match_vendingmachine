import { Request, Response } from "express"
import { Product } from "../../models/Product";


export default async function getProducts(req: Request, res: Response) {
  try {
    let productId = req.params.id

    // check type of params
    if (productId != null) {
      // get one
      if (!(typeof productId === "string")) {
        res.json({
          success: false,
          error: "false params"
        })
        return
      }

      let product = await Product.findOne({ _id: productId })
      if (product == null) {
        res.json({
          success: false,
          error: "product does not exist"
        })
        return
      }

      res.json({
        success: true,
        product: product
      })
      return
    }

    // get all products
    let products = await Product.find({})
    res.json({
      success: true,
      products: products
    })
  } catch (error) {
    console.log(error)

    res.json({
      success: false,
      error: error
    })
  }
}