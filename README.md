# MVP Match - Vending Machine

## Server (./nodejs-express-mongodb)
```
npm run dev
```

### Dependencies
- node.js
- Express
- TypeScript
- MongoDB (Mongoose)
- JWT for Authentication-Token und Refresh-Token

### Requests
- Register
- Login
- Get Auth-Token from Refresh-Token
- Logout
- Logout all current Sessions
- Get User
- Edit User
- Get Product(s)
- Deposit (User)
- Buy Product(s) (User)
- Add Product (Seller)
- Edit Product (Seller)
- Delete Product (Seller)

### Automated Tests
- Deposit
- Deposit with negative value
- Buy single Product
- Buy Product without enough Deposit
- Buy Product with negative Quantity
- Buy Product which is not available anymore (amountAvailable == 0)
- Add Product
- Add Product as unauthorized user
- Add Product without valid params/data (no params, empty name, empty cost, cost < 0, amountavailable empty, cost % 5 != 0)
- Add Product with existing Name
- Edit Product (change Name, change Cost, change amountAvailable, change Cost < 0, Cost % 5 != 0)
- Edit Product of another Seller
- Delete Product
- Delete Product of another Seller


## Front End (./vue3)
```
npm run watch
```

### Dependencies
- Vue3 (TypeScript)
- JWT decode
- axios for HTTP Requests
- Bootstrap for Styling

